% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/validate_df.R
\name{validate_df}
\alias{validate_df}
\title{validate the data frame}
\usage{
validate_df(x)
}
\arguments{
\item{x}{the data frame}
}
\description{
validate the data frame
}
\author{
Jörg Henke
}
