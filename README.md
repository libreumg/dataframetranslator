# dataframetranslator

a library to translate well defined data frames with the google translation api

call it by run("my_xls_file_path")

To reduce costs, you might consider to use the param dry_run to set to TRUE; with that, the google translation api is NOT called and all the missing translations are replaced by the candidates in upper case. This way, one might test the functionality of the library.
